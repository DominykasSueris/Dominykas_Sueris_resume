const button = document.getElementById("contactButton");
const content = document.getElementById("modal");
const closeButton = document.getElementById("close-button");
const overlay = document.getElementById("overlay");


button.onclick = function() {
    content.style.display = "block";
    overlay.style.display = "block";
}

closeButton.onclick = function() {
    content.style.display = "none";
    overlay.style.display = "none"
}